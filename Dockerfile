FROM debian:bullseye

ARG LLVM_VERSION=19

ARG APP_UID=2000

ARG APP_GID=2000

LABEL maintainer="TANGO Controls team <contact@tango-controls.org>"

ENV DEBIAN_FRONTEND=noninteractive

ENV PKG_CONFIG_PATH=/home/tango/lib/pkgconfig
ENV CMAKE_PREFIX_PATH=/home/tango/lib/cmake
ENV CXX=clang++-${LLVM_VERSION}
ENV CC=clang-${LLVM_VERSION}
ENV RUN_CLANG_TIDY=run-clang-tidy-${LLVM_VERSION}

RUN apt-get update -qq &&                      \
    apt-get install -y --no-install-recommends \
      apt-utils                                \
      build-essential                          \
      bzip2                                    \
      ca-certificates                          \
      cmake                                    \
      curl                                     \
      gdb                                      \
      git                                      \
      gnupg                                    \
      libc-ares-dev                            \
      libcurl4-openssl-dev                     \
      libjpeg-dev                              \
      libre2-dev                               \
      libssl-dev                               \
      libzmq3-dev                              \
      libzstd-dev                              \
      pkg-config                               \
      python3                                  \
      python3-dev                              \
      ruby-dev                                 \
      sudo                                     \
      zlib1g-dev &&                            \
      rm -rf /var/lib/apt/lists/*

RUN apt-get update -qq &&                                                     \
    apt-get install -y --no-install-recommends                                \
        lsb-release wget software-properties-common gnupg &&                  \
    curl -LOJR https://apt.llvm.org/llvm.sh &&                                \
    chmod +x llvm.sh &&                                                       \
    ./llvm.sh ${LLVM_VERSION} &&                                              \
    rm llvm.sh &&                                                             \
    apt-get install -y --no-install-recommends                                \
        clang-format-${LLVM_VERSION}                                          \
        clang-tidy-${LLVM_VERSION}                                            \
        libc++-${LLVM_VERSION}-dev                                            \
        libc++abi-${LLVM_VERSION}-dev &&                                      \
    rm -rf /var/lib/apt/lists/* &&                                            \
    ln -s /usr/lib/llvm-${LLVM_VERSION}/bin/llvm-symbolizer /usr/local/bin && \
    ln -s /usr/lib/x86_64-linux-gnu /usr/lib/x86_64-pc-linux-gnu

RUN groupadd -g "$APP_GID" tango                                && \
    useradd -l -u "$APP_UID" -g "$APP_GID" -ms /bin/bash tango  && \
    usermod -a -G sudo tango                                    && \
    echo "%sudo ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers         && \
    echo "/home/tango/lib" > /etc/ld.so.conf.d/home.conf

USER tango

WORKDIR /home/tango

COPY --chown="$APP_UID":"$APP_GID" scripts/*.sh ./
COPY --chown="$APP_UID":"$APP_GID" scripts/patches/* ./patches/

RUN ./common_setup.sh               && \
    ./install_codeclimate.sh        && \
    ./install_cppzmq.sh             && \
    ./install_tango_idl.sh          && \
    ./install_omniorb.sh            && \
    ./install_catch.sh              && \
    ./install_opentelemetry_deps.sh && \
    ./install_opentelemetry.sh
# keep the dependencies folder as the clang-tidy job requires it
